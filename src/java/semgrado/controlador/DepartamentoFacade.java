/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import semgrado.entidades.Departamento;

/**
 *
 * @author edwardceballos
 */
@Stateless
public class DepartamentoFacade extends AbstractFacade<Departamento> {

    @PersistenceContext(unitName = "semgradoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepartamentoFacade() {
        super(Departamento.class);
    }
    
            
    public List obtenerNinosDepartamento()
    {
        Query q = em.createNamedQuery("Departamento.findDepartamentosInfantes",Departamento.class);        
        List<Object[]> mayorAccidente = q.getResultList();
        return mayorAccidente;
    }
    
}
