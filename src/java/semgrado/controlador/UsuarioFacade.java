/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import semgrado.entidades.Usuario;

/**
 *
 * @author edwardceballos
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "semgradoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario obtenerUsuarioxEmail(String correoUser){
        try {
            Query q= em.createNamedQuery("Usuario.findByEmail", Usuario.class).setParameter("email", correoUser);
            Usuario user = (Usuario) q.getSingleResult();
            return user;
        } catch (NoResultException e) {
            return null;
        }
    }  
}
