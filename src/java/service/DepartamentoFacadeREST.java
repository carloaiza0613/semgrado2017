/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import semgrado.controlador.UsuarioFacade;
import semgrado.entidades.Departamento;
import semgrado.entidades.dto.RespuestaDto;
import semgrado.entidades.excepciones.EntidadesException;
import semgrado.entidades.validadores.ValidadorDepartamento;

/**
 *
 * @author carloaiza
 */
@Stateless
@Path("semgrado.entidades.departamento")
public class DepartamentoFacadeREST extends AbstractFacade<Departamento> {

    @EJB
    private UsuarioFacade usuarioFacade;
    
    
    @PersistenceContext(unitName = "semgradoPU")
    private EntityManager em;

    public DepartamentoFacadeREST() {
        super(Departamento.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDto createDepto(Departamento entity,@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password ) {
        try {
            ValidadorDepartamento.validarAutenticacion(authentication, password, usuarioFacade);  
             ValidadorDepartamento.validarIdExistente(entity.getCodigo(), this);
             ValidadorDepartamento.validarCamposObligatorios(entity);
            super.create(entity);
       return new RespuestaDto("201","Se ha registrado satisfactoriamente");
        } catch (EntidadesException ex) {
           return new RespuestaDto("406", ex.getMessage());
        }

    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDto editDepto(Departamento entity) {
        
        try {
            ValidadorDepartamento.validarIdNoExistente(entity.getCodigo(), this);
            ValidadorDepartamento.validarCamposObligatorios(entity);           
            super.edit(entity);
            return new RespuestaDto("201","Se ha modificado satisfactoriamente");
            
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());
        }
    }

    @DELETE
    @Path("{id}")
    public RespuestaDto remove(@PathParam("id") Integer id) {
        try {
            ValidadorDepartamento.validarIdNoExistente(id, this);
            super.remove(super.find(id));
            return new RespuestaDto("201","Se ha elimnado el departamento");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Departamento find(@PathParam("id") Integer id, @HeaderParam("auth") String authentication) {

        return super.find(id);
    }

    @GET   
    @Produces({MediaType.APPLICATION_JSON})
    public List<Departamento> findAll(@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password) {        
        
        try {
            ValidadorDepartamento.validarAutenticacion(authentication, password, usuarioFacade);            
            return super.findAll();
        } catch (EntidadesException ex) {
            Logger.getLogger(DepartamentoFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Departamento> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
