/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import semgrado.entidades.Departamento;
import semgrado.entidades.Infante;
import semgrado.entidades.Municipio;

/**
 *
 * @author edwardceballos
 */
@Stateless
public class MunicipioFacade extends AbstractFacade<Municipio> {

    @PersistenceContext(unitName = "semgradoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MunicipioFacade() {
        super(Municipio.class);
    }
    
    public List<Municipio> municipiosList(Departamento departamento){
        System.out.println("departamento = " + departamento.getCodigo());
        Query q = em.createNamedQuery("Municipio.findByIdDepartament",Municipio.class);
        q.setParameter("departamentId", departamento);
        
        List<Municipio> listMunicipio = q.getResultList();
        return listMunicipio;
    }
        
    public List obtenerNinosMunicipios()
    {
        /*Query q = em.createNativeQuery("SELECT mun.nombre as municipio, count(*) as cantidad\n" +
                                        "FROM infante as inf\n" +
                                        "INNER JOIN municipio as mun\n" +
                                        "ON inf.fk_municipio = mun.codigo\n" +
                                        "GROUP BY inf.fk_municipio, mun.nombre");*/
        
        Query q = em.createNamedQuery("Municipio.findInfanteMunicipio",Municipio.class);
        
        List<Object[]> mayorAccidente = q.getResultList();
        return mayorAccidente;
    }
}
