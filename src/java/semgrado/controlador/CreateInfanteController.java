/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import java.io.Serializable;
import javax.ejb.EJB;
import semgrado.controlador.util.JsfUtil;
import semgrado.entidades.Departamento;
import semgrado.entidades.Infante;

/**
 *
 * @author edwardceballos
 */
public class CreateInfanteController implements Serializable{
        
    @EJB
    private InfanteFacade infanteFacade;
    
    private Infante infante;
    
    private Departamento selected;
       
    /**
     * Creates a new instance of CreateInfanteController
     */
    public CreateInfanteController() {
        infante = new Infante();
    }
    
    public Infante getInfante() {
        return infante;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }
    
    public void limpiarFormulario(){
        infante =  new Infante();
        selected =  new Departamento();
    }
    
    public Departamento getSelected() {
        return selected;
    }

    public void setSelected(Departamento selected) {
        this.selected = selected;
    }
  
    public void guardarInfante(){
        try {
            infanteFacade.create(infante);
            JsfUtil.addSuccessMessage("Infante guardado con èxito");
            limpiarFormulario();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e.getCause().toString());
        }
    }
    
}
