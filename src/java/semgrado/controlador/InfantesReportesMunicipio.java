/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author edwardceballos
 */
@Named(value = "infantesReportesMunicipio")
@SessionScoped
public class InfantesReportesMunicipio implements Serializable {
    
    @EJB
    private semgrado.controlador.MunicipioFacade municipioFacade;
    
    private List<Object[]> ninosMunicipio;
    
    private PieChartModel infanteXMunicipio;  

    public PieChartModel getInfanteXMunicipio() {
        return infanteXMunicipio;
    }

    /**
     * Creates a new instance of ninos municipio
     */
    public InfantesReportesMunicipio() {
    }

    public List<Object[]> getNinosMunicipio() {
        return ninosMunicipio;
    }

    public void setNinosMunicipio(List<Object[]> ninosMunicipio) {
        this.ninosMunicipio = ninosMunicipio;
    }

    @PostConstruct
    public void obtenerNinosMunicipios(){
        ninosMunicipio = municipioFacade.obtenerNinosMunicipios();
        		
        infanteXMunicipio = new PieChartModel();
        
        for(Object[] item : ninosMunicipio){
            infanteXMunicipio.set(item[0].toString(),Double.parseDouble(item[1].toString()));
        } 
          
        infanteXMunicipio.setTitle("Niños Por Municipio");
        infanteXMunicipio.setLegendPosition("e");
        infanteXMunicipio.setFill(false);
        infanteXMunicipio.setShowDataLabels(true);
        infanteXMunicipio.setDiameter(150);
    }
}
