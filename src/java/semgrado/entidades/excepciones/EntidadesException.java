/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.entidades.excepciones;

/**
 *
 * @author carloaiza
 */
public class EntidadesException extends Exception{

    public EntidadesException() {
    }

    public EntidadesException(String message) {
        super(message);
    }

    public EntidadesException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntidadesException(Throwable cause) {
        super(cause);
    }

    
    
}
