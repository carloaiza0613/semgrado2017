/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.entidades.validadores;

import semgrado.controlador.DepartamentoFacade;
import semgrado.controlador.UsuarioFacade;
import semgrado.entidades.Departamento;
import semgrado.entidades.Usuario;
import semgrado.entidades.excepciones.EntidadesException;
import service.DepartamentoFacadeREST;

/**
 *
 * @author carloaiza
 */
public class ValidadorDepartamento {
    
    public static void validarIdExistente(int id, DepartamentoFacadeREST deptoFacade) 
            throws EntidadesException
    {
        if(deptoFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ingresado ya existe");
        }    
    }
    
    public static void validarIdNoExistente(int id, DepartamentoFacadeREST deptoFacade) 
            throws EntidadesException
    {
        if(deptoFacade.find(id)==null)
        {
            throw new EntidadesException("El id ingresado no existe");
        }    
    }
    
    public static void validarCamposObligatorios(Departamento depto) 
            throws EntidadesException
    {
        if(depto.getNombre()==null || depto.getNombre().equals(""))
        {
            throw new EntidadesException("Debe diligenciar el nombre del departamento");
        }    
    }
    
    
    public static void validarAutenticacion(String auth, String passwd, UsuarioFacade usuFacade)
            throws EntidadesException
    {
        Usuario usu= usuFacade.obtenerUsuarioxEmail(auth);
        
        if(usu!=null)
        {    
            if(!auth.equals(usu.getEmail()) || !passwd.equals(usu.getPassword()))
            {
                throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
            }    
        }
        else
        {
           throw new EntidadesException("Los datos de autenticación son erróneos"); 
        }
        
    }        
}
