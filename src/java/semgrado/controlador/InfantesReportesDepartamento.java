/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author edwardceballos
 */
@Named(value = "infantesReportesDepartamento")
@ViewScoped
public class InfantesReportesDepartamento implements Serializable {
    
    @EJB
    private DepartamentoFacade dptoFacade;
    
    private List<Object[]> ninosDepartamento;
    
    private PieChartModel infanteXDepartamento;  

    /**
     * Creates a new instance of ninos municipio
     */
    public InfantesReportesDepartamento() {
    }

    @PostConstruct
    public void obtenerNinosDepartamento(){
        ninosDepartamento = dptoFacade.obtenerNinosDepartamento();
        		
        infanteXDepartamento = new PieChartModel();
        
        for(Object[] item : ninosDepartamento){
            infanteXDepartamento.set(item[0].toString(),Double.parseDouble(item[1].toString()));
        } 
          
        infanteXDepartamento.setTitle("Niños Por Departamentos");
        infanteXDepartamento.setLegendPosition("e");
        infanteXDepartamento.setFill(false);
        infanteXDepartamento.setShowDataLabels(true);
        infanteXDepartamento.setDiameter(150);
    }

    public List<Object[]> getNinosDepartamento() {
        return ninosDepartamento;
    }

    public void setNinosDepartamento(List<Object[]> ninosDepartamento) {
        this.ninosDepartamento = ninosDepartamento;
    }

    public PieChartModel getInfanteXDepartamento() {
        return infanteXDepartamento;
    }

    public void setInfanteXDepartamento(PieChartModel infanteXDepartamento) {
        this.infanteXDepartamento = infanteXDepartamento;
    }
    
}
