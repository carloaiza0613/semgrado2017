/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import semgrado.entidades.Departamento;
import semgrado.entidades.Municipio;

/**
 *
 * @author edwardceballos
 */
@Named(value = "appController")
@ApplicationScoped
public class AppController {
    
    @EJB
    private DepartamentoFacade dptoFacade;
    
    @EJB
    private MunicipioFacade municipioFacade;
    
    private List<Departamento> listDepartamentos;
    
    private List<Municipio> listMunicipio;
    /**
     * Creates a new instance of AppController
     */
    public AppController() {
    }
    
    @PostConstruct
    public void llenarCombos(){
        listDepartamentos = dptoFacade.findAll();
        listMunicipio = municipioFacade.findAll();
    }

    public List<Municipio> obtenerMunicipioPorDepartamento (Departamento dptoSelect){
        
        List<Municipio> lista = new ArrayList<>();

        if(dptoSelect != null){
        
            for (Municipio mun:getListMunicipio()) {
                if (dptoSelect.equals(mun.getFkDepartamento())) {
                    lista.add(mun);       
                }
            }
        }
        return lista;
    }
    
    public List<Departamento> getListDepartamentos() {
        return listDepartamentos;
    }

    public void setListDepartamentos(List<Departamento> listDepartamentos) {
        this.listDepartamentos = listDepartamentos;
    }

    public List<Municipio> getListMunicipio() {
        return listMunicipio;
    }

    public void setListMunicipio(List<Municipio> listMunicipio) {
        this.listMunicipio = listMunicipio;
    }
}
