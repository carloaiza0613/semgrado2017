/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semgrado.controlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import semgrado.controlador.util.Encriptar;
import semgrado.entidades.Usuario;

/**
 *
 * @author edwardceballos
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {
    
    private String correoUsuario ="";
    private String password = "";
    
    @EJB
    private UsuarioFacade usuarioFacade;
   
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
    public String autenticar() throws NoSuchAlgorithmException, Exception{
        
        Usuario usuarioAutenticado = usuarioFacade.obtenerUsuarioxEmail(correoUsuario);
       
        if (usuarioAutenticado != null) {
            
            //String userContrasenaIngresada = Encriptar.getStringMessageDigest(password, Encriptar.MD5);
            
            if (password.equals(usuarioAutenticado.getPassword())) {
                return "ingresar";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Debe ingresar un password valido"));
                return null;
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Correo invalido"));
        return null;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
