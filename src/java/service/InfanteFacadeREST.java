/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import semgrado.controlador.UsuarioFacade;
import semgrado.entidades.Infante;
import semgrado.entidades.Usuario;
import semgrado.entidades.excepciones.EntidadesException;

/**
 *
 * @author carloaiza
 */
@Stateless
@Path("semgrado.entidades.infante")
public class InfanteFacadeREST extends AbstractFacade<Infante> {

    @PersistenceContext(unitName = "semgradoPU")
    private EntityManager em;

    @EJB
    private UsuarioFacade usuFacade;

    public InfanteFacadeREST() {
        super(Infante.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Infante entity) {
        try {
            super.create(entity);
        } catch (Exception ex) {
            Logger.getLogger(InfanteFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") String id, Infante entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Infante find(@PathParam("id") String id) {
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAll(@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password) {

        Usuario usu = usuFacade.obtenerUsuarioxEmail(authentication);
        if (usu != null) {
            if (!authentication.equals(usu.getEmail()) || !password.equals(usu.getPassword())) {
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("{mensaje:No tienes permiso}").build();
            }
        } else {
            return Response.serverError().entity("{mensaje:Datos de autenticación erróneos}").build();
        }
        
        List<Infante> list = super.findAll();
        
         GenericEntity<List<Infante>> lista = new GenericEntity<List<Infante>>(list) {
        };
        
        return Response.ok(lista, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Infante> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
